import React from "react";
import { createStyles, Group, Navbar } from "@mantine/core";
import { MantineLogo } from "../img/MantineLogo";
import { SearchInput } from "./SearchInput";
import { ContainedInputs } from "./GenreDropdown";

const useStyles = createStyles((theme, _params, getRef) => {
    const icon = getRef("icon");
    return {
        navbar: {
            backgroundColor: theme.colors[theme.primaryColor][6]
        },

        version: {
            backgroundColor: theme.colors[theme.primaryColor][7],
            color: theme.white,
            fontWeight: 700
        },

        header: {
            paddingBottom: theme.spacing.md,
            marginBottom: theme.spacing.md * 1.5,
            borderBottom: `1px solid ${theme.colors[theme.primaryColor][7]}`
        },

        footer: {
            paddingTop: theme.spacing.md,
            marginTop: theme.spacing.md,
            borderTop: `1px solid ${theme.colors[theme.primaryColor][7]}`
        },

        link: {
            ...theme.fn.focusStyles(),
            display: "flex",
            alignItems: "center",
            textDecoration: "none",
            fontSize: theme.fontSizes.sm,
            color: theme.white,
            padding: `${theme.spacing.xs}px ${theme.spacing.sm}px`,
            borderRadius: theme.radius.sm,
            fontWeight: 500,

            "&:hover": {
                backgroundColor: theme.colors[theme.primaryColor][5]
            }
        },

        linkIcon: {
            ref: icon,
            color: theme.white,
            opacity: 0.75,
            marginRight: theme.spacing.sm
        },

        linkActive: {
            "&, &:hover": {
                backgroundColor: theme.colors[theme.primaryColor][7],
                [`& .${icon}`]: {
                    opacity: 0.9
                }
            }
        }
    };
});

// TODO: hide navbar when viewport is too small
export function CustomNavbar() {
    const { classes, cx } = useStyles();

    return (
        <Navbar fixed position={{ top: 0, left: 0 }} width={{ sm: 300 }} p="md" className={classes.navbar}>
            <Navbar.Section grow>
                <Group className={classes.header} position="apart">
                    <MantineLogo variant="white" />
                </Group>
                <SearchInput />
                <ContainedInputs />
            </Navbar.Section>
        </Navbar>
    );
}
