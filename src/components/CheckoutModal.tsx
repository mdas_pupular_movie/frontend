import { Button, Group, Modal, Paper, TextInput, Title } from "@mantine/core";

export function CheckoutModal(props: {
    opened: boolean;
    onSubmit: () => void;
    onClose: () => void;
    onClick: () => void;
}) {
    return (
        <Modal
            title={<Title order={3}>Checkout</Title>}
            opened={props.opened}
            onSubmit={props.onSubmit}
            onClose={props.onClose}
        >
            <Paper p="lg">
                <form>
                    <TextInput label="Email" radius="md" />
                    <Group position="left" grow>
                        <TextInput label="Name" radius="md" />
                        <TextInput label="Expiration" radius="md" />
                    </Group>
                    <Group position="left" grow>
                        <TextInput label="Card Number" radius="md" />
                        <TextInput label="CVV" radius="md" />
                    </Group>
                    <Group position="right" style={{ marginTop: 15 }}>
                        <Button onClick={props.onClick}>Buy Now</Button>
                    </Group>
                </form>
            </Paper>
        </Modal>
    );
}
