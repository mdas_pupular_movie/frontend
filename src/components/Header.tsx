import React from "react";
import { Container, createStyles, Header, Title } from "@mantine/core";
import { CartMenu } from "./CartMenu";

const useStyles = createStyles((theme) => ({
    inner: {
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center",
        color: "white",
        maxWidth: "unset!important",
        height: 56,
        [theme.fn.smallerThan("sm")]: {
            justifyContent: "flex-start"
        }
    },
    title: {
        backgroundColor: theme.colors[theme.primaryColor][6],
        borderBottom: "unset!important",
    },
    titleComp: {
        width: "100%",
        textAlign: "center"
    }
}));

export function HeaderMiddle() {
    const { classes } = useStyles();

    return (
        <Header className={classes.title} height={56} mb={120}>
            <Container className={classes.inner}>
                <Title order={1} className={classes.titleComp}>Popular Movie Store</Title>
                <CartMenu />
            </Container>
        </Header>
    );
}
