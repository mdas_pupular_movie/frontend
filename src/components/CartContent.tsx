import { ActionIcon, Box, Button, Center, Divider, Group, Table, Text } from "@mantine/core";
import { Trash } from "tabler-icons-react";
import { Cart } from "../definitions/cart";

interface CartContentProps {
    cart: Cart;

    onSubmit(): void;

    onCancel(): void;

    onRemoveMovie(id: string, quantity: number): void;
}
export function CartContent({ cart, onSubmit, onRemoveMovie }: CartContentProps) {
    const rows = cart.moviesList.map((cartMovieList) => (
        <tr key={cartMovieList.id}>
            <td>
                <Group spacing="sm">
                    <Text size="sm" weight={500}>
                        {cartMovieList.title}
                    </Text>
                </Group>
            </td>
            <td>
                <Group spacing="sm">{cartMovieList.price}</Group>
            </td>
            <td>
                <Group spacing="sm">{cartMovieList.quantity}</Group>
            </td>
            <td>
                <Center inline>
                    <ActionIcon
                        onClick={() => {
                            onRemoveMovie(cartMovieList.id, cartMovieList.quantity || 0);
                        }}
                        size="md"
                        color="red"
                    >
                        <Trash />
                    </ActionIcon>
                </Center>
            </td>
        </tr>
    ));
    return (
        <form onSubmit={onSubmit}>
            <Table sx={{ minWidth: 800 }} verticalSpacing="sm">
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Price</th>
                        <th>Quantity</th>
                        <th>Remove</th>
                    </tr>
                </thead>
                <tbody>{rows}</tbody>
            </Table>
            <Divider
                my="sm"
                variant="dashed"
                labelPosition="center"
                label={
                    <>
                        <Box ml={5}>END</Box>
                    </>
                }
            />

            <Text weight={700} align="right">
                Total: {cart.totalPrice?.toLocaleString(undefined, { maximumFractionDigits: 2 })} €
            </Text>

            <Group position="right" style={{ marginTop: 15 }}>
                <Button
                    onClick={() => {
                        onSubmit();
                    }}
                >
                    Buy Now
                </Button>
            </Group>
        </form>
    );
}
