import { Pagination as MantinePagination } from "@mantine/core";
import { useAppDispatch } from "../app/hooks";
import { getMoviesPage } from "../features/movie/movie_slice";

interface PaginationProps {
    currentPage: number;
    totalPages: number;
}

export function Pagination({ currentPage, totalPages }: PaginationProps) {
    const dispatch = useAppDispatch();
    return (
        <MantinePagination
            page={currentPage}
            total={totalPages}
            radius="md"
            withEdges
            onChange={(page) => {
                dispatch(getMoviesPage({ page }));
            }}
        />
    );
}
