import { Action, configureStore, ThunkAction } from "@reduxjs/toolkit";
import movieReducer from "../features/movie/movie_slice";
import cartReducer from "../features/cart/cart_slice";
import counterReducer from "../features/counter/counterSlice";
import { loadState, saveState } from "./localStorage";

const preloadedState = loadState();
export const store = configureStore({
    reducer: {
        movies: movieReducer,
        cart: cartReducer,
        counter: counterReducer
    },
    preloadedState
});

store.subscribe(() => {
    saveState({
        cart: store.getState().cart
    });
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<ReturnType, RootState, unknown, Action<string>>;
