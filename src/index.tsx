import React from "react";
import { createRoot } from "react-dom/client";
import { Provider } from "react-redux";
import { store } from "./app/store";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import "./index.css";
import { getMovies } from "./features/movie/movie_slice";
import { createCart, getCart } from "./features/cart/cart_slice";

const container = document.getElementById("root")!;
const root = createRoot(container);
store.dispatch(getMovies({ page: 1, searchBy: "title", searchTearm: "" }));
const cartStore = store.getState().cart;
if (cartStore.cart === null) {
    store.dispatch(createCart());
} else {
    store.dispatch(getCart({ cartId: cartStore.cart.cartId }));
}

root.render(
    <React.StrictMode>
        <Provider store={store}>
            <App />
        </Provider>
    </React.StrictMode>
);

reportWebVitals();
