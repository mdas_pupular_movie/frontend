import React from "react";
import { Grid } from "@mantine/core";
import { MovieCard } from "../../components/MovieCard";
import { Movie } from "../../definitions/movie";
import { useAppSelector } from "../../app/hooks";
import { selectMovies } from "../../features/movie/movie_slice";
import { selectCart } from "../../features/cart/cart_slice";

// TODO: change amount of cards on smaller viewports 5 -> 1
export function MovieList() {
    const movie_store = useAppSelector(selectMovies);
    const cart_store = useAppSelector(selectCart);
    const movie_cards = movie_store.results.map((movie: Movie) => (
        <Grid.Col key={movie.id} md={3} lg={2}>
            <MovieCard movie={movie} cartId={cart_store?.cart?.cartId} />
        </Grid.Col>
    ));

    return (
        <div>
            <div>
                <Grid>{movie_cards}</Grid>
            </div>
        </div>
    );
}
