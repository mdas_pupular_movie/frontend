import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { RootState } from "../../app/store";
import * as CartAPI from "./cart_api";
import {
    AddMovieToCartParams,
    CartState,
    DeleteCartParams,
    GetCartParams,
    RemoveMovieFromCartParams
} from "../../definitions/cart";

const initialState: CartState = {
    cart: null,
    status: "idle"
};

export const createCart = createAsyncThunk("cart/createCart", async () => {
    const response = await CartAPI.createCart();
    return response.data;
});

export const getCart = createAsyncThunk("cart/getCart", async (params: GetCartParams) => {
    const response = await CartAPI.getCart(params);
    return response.data;
});

export const addMovieToCart = createAsyncThunk("cart/addMovieToCart", async (params: AddMovieToCartParams) => {
    await CartAPI.addMovieToCart(params);
    const response = await CartAPI.getCart({ cartId: params.cartId });
    return response.data;
});

export const removeMovieFromCart = createAsyncThunk(
    "cart/removeMovieFromCart",
    async (params: RemoveMovieFromCartParams) => {
        await CartAPI.removeMovieFromCart(params);
        setTimeout(async () => {
            await getCart({ cartId: params.cartId });
        }, 250);
    }
);

export const deleteCart = createAsyncThunk("cart/deleteCart", async (params: DeleteCartParams) => {
    await CartAPI.deleteCart(params);
});

export const cartSlice = createSlice({
    name: "cart",
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(createCart.pending, (state) => {
                state.status = "loading";
            })
            .addCase(createCart.fulfilled, (state, action) => {
                state.cart = action.payload;
                state.status = "idle";
            })
            .addCase(createCart.rejected, (state) => {
                state.status = "failed";
            })

            .addCase(addMovieToCart.pending, (state) => {
                state.status = "loading";
            })
            .addCase(addMovieToCart.fulfilled, (state, action) => {
                state.cart = action.payload;
                state.status = "idle";
            })
            .addCase(addMovieToCart.rejected, (state) => {
                state.status = "failed";
            })
            .addCase(removeMovieFromCart.pending, (state) => {
                state.status = "loading";
            })
            .addCase(removeMovieFromCart.fulfilled, (state) => {
                state.status = "idle";
            })
            .addCase(removeMovieFromCart.rejected, (state) => {
                state.status = "failed";
            })
            .addCase(getCart.pending, (state) => {
                state.status = "loading";
            })
            .addCase(getCart.fulfilled, (state, action) => {
                state.cart = action.payload;
                state.status = "idle";
            })
            .addCase(getCart.rejected, (state) => {
                state.cart = null;
                state.status = "idle";
            })

            .addCase(deleteCart.pending, (state) => {
                state.status = "loading";
            })
            .addCase(deleteCart.fulfilled, (state) => {
                state.cart = initialState.cart;
                state.status = "idle";
            })
            .addCase(deleteCart.rejected, (state) => {
                state.status = "failed";
            });
    }
});

export const selectCart = (state: RootState) => state.cart;

export default cartSlice.reducer;
