import { GetMovieByIdParams, GetMoviesParams, Movie, MoviesResult } from "../../definitions/movie";
import axios from "axios";
import { BASE_URL, SEARCH_LIMIT } from "../../definitions/const";

const instance = axios.create({
    baseURL: BASE_URL
});
const get_movie_params_default: GetMoviesParams = { page: 1, searchBy: "title", searchTearm: "" };

export async function getMovies(params: GetMoviesParams = get_movie_params_default): Promise<{ data: MoviesResult }> {
    const result = await instance.post("/movies", { ...params, limit: SEARCH_LIMIT });
    return { data: result.data };
}

export async function getMovieById(params: GetMovieByIdParams): Promise<{ data: Movie }> {
    const result = await instance.post(`/movies/${params.id}`);
    return { data: result.data };
}
